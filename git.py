import requests


response = requests.get("https://api.github.com/repos/Suya-Hime-Suki/Song-for-Prism-Replace-Assets/releases/latest")
response.raise_for_status() 
assets = response.json()["assets"]


for asset in assets[:2]: 
    download_url = asset["browser_download_url"]
    file_name = download_url.split("/")[-1]

   
    print(f"Downloading {file_name}...")
    response = requests.get(download_url, stream=True)
    response.raise_for_status()
    
    with open(file_name, "wb") as f:
        for chunk in response.iter_content(chunk_size=8192):
            f.write(chunk)
    
    print(f"{file_name} has been downloaded.")

print("The first and second files have been downloaded.")